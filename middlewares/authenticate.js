const jwt = require('jsonwebtoken');
const {success, error} = require('./response')
const User = require('../models/user')

module.exports = function (req, res, next){
    const token = req.header('authorization')
    if(!token) return res.status(401).json(error("can't put from header"))

    try {
        const verified = jwt.verify(token, process.env.SECRET_KEY)
        req.user = verified
        User.findById(req.user, (err, user)=>{
            if (!user) return res.status(403).json(error("there is no user found"));
            else next()
        })
    }

    catch (err){
        res.status(401).json(error("Invalid Token"))
    }
}