const validator = require('fastest-validator')
const v = new validator()

const schema = {
    title: { type: "string", required: true },
    body: { type: "string", optional: true },
    isDone: { type: "boolean", optional: true, convert: true } // short-hand def
};

var todo = {
    create(data){
        let check = v.compile(schema)
        return check(data)
    },
    update(data){
        for (var i in schema){
            delete schema[i].required
            schema[i].optional=true
        }
        let check = v.compile(schema)
        return check(data)
    }}
    
    module.exports=todo;