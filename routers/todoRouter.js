const router = require('express').Router()
const todoController = require('../controllers/todoController')
const authenticate = require('../middlewares/authenticate')

router.post('/', authenticate, todoController.create)
router.get('/', authenticate, todoController.showAll)
router.put('/:id', authenticate, todoController.update)
router.get('/:id', authenticate, todoController.show)
router.delete('/:id', authenticate, todoController.delete)

module.exports = router