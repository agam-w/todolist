const router = require('express').Router()
const userController = require('../controllers/userController')
const authenticate = require('../middlewares/authenticate')

router.post('/', userController.create);
router.post('/login', userController.login);
router.get('/', authenticate, userController.show)
router.delete('/', authenticate, userController.delete)
router.get('/verify/:token', userController.verifyEmail)
router.post('/forget/', userController.forgetPassword)
router.put('/forget/:token', userController.changePassword)
module.exports = router
