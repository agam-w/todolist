const express = require('express')
const app = express()
const morgan = require('morgan')
var cors = require('cors')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(morgan('tiny'))

const mongoose = require('mongoose')
const env = process.env.NODE_ENV || 'development';

if (env == 'development' || env == 'test'
) {
  require('dotenv').config()
}

const configDb = {
  development: process.env.DBDEV,
  test: process.env.DBTEST,
  staging: process.env.DBCONNECTION
}

mongoose.connect(configDb[env], { useNewUrlParser: true, useCreateIndex: true })
  .then(() => console.log("connect DB"))

const router = require('./routers');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api', router);

app.get('/', (req, res) => {
  res.status(200).json({
    success: true,
    message: `Welcome to ${env}!`
  });
});


const port = process.env.PORT

app.listen(port, () => {
  console.log(`Server Started at ${Date()}!`)
  console.log(`Listening on port ${port} in database ${env}!`)
})

module.exports = app