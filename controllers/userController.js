const User = require('../models/user')
const jwt = require('jsonwebtoken');
const funcHelper = require('../middlewares/funcHelper')
// const nodemailer = require('nodemailer');

const {success, error} = require('../middlewares/response')

// var transporter = nodemailer.createTransport({
//     service: 'gmail',
//     auth: {
//       user: 'agamw215@gmail.com',
//       pass: 'akuadalahaku'
//     }
//   });

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = {
    create(req, res){
        // var mailOptions = {
        //     from: 'agamw215@gmail.com',
        //     to: req.body.email,
        //     subject: 'Hallo my friends',
        //     html: `<h1>Welcome</h1><p>http://localhost:7000/api/user/${req.body.email}</p>`
        //     }
        var token = funcHelper.token(20);
        req.body['token']       = token;
        req.body['expToken']    = new Date(new Date().setHours(new Date().getHours() + 6))
        
        User.create(req.body, (err, data)=>{
            if(err) return res.status(422).json(error(err.message))
            
            var to              = req.body.email
            var from            = 'abc-todo@example.com'
            var subject         = 'Verify your main in abc-todo';

            var link            = "http://"+req.get('host')+"/api/user/verify/"+token;
            var html            = 'Plese click link bellow, if you register at todoglint.com<br>';
                html            += '<br><strong><a href='+link+'>'+link+'</a></strong>';
                html            += '<br><br>Thanks';
                
            funcHelper.mail(to, from, subject, html)
            res.status(201).json(success(data, "user created"))
            
            // transporter.sendMail(mailOptions, function(error, info){
            //     if (error) {
            //       res.status(400).json(error);
            //     } else {
            //       console.log('Email sent: ' + info.response);
            //     }
              // });
        })
    },
    login(req, res){
        User.findOne({email: req.body.email}, (err, user)=>{
            if (user) {
                if (user.password==req.body.password){
                        jwt.sign({ _id: user._id }, process.env.SECRET_KEY, function(err, token) {
                        res.status(200).set('Authorization', token).json(success(token, "token created"));
                    });
                }
                else res.status(403).json(error("incorrect password"));
                
            } else {
                res.status(403).json(error("invalid email"));
            }
        })
    },
    show(req, res){
        User.findById(req.user,(err, user)=>{
            if (err) res.status(403).json(error(err.message))
            res.status(200).json(success(user, "Show current user"))
        })
    },
    delete(req, res){
        User.findByIdAndDelete(req.user).exec(result=>{
            res.status(200).json(success(result, "success delete user"))
      })
    },
    verifyEmail(req, res){
        User.findOne({ token: token }, 'expToken').exec()
        .then((users)=>{
            if(Date.now()<users.expToken){
                User.findOneAndUpdate({token: req.params.token}, {isVerified: true}, (err, data)=>{
                    if (!data) return res.status(400).json(error("the token is not exist"))
                    res.status(200).json(success(data, "email verified success"))
                })
            }
            else{
                res.status(422).json(FuncHelpers.errorResponse('Time token validations is expired, please resend email confirm'))
            }
        })
        .catch((err)=>{
            res.status(422).json(err)
        })
    },
    forgetPassword(req, res){
        var token = funcHelper.token(20);
        req.body['token']   = token;
        req.body['expToken'] = new Date(new Date().setHours(new Date().getHours() + 6))
        User.findOneAndUpdate({email: req.body.email}, req.body, (err, data)=>{
            if (!data) return res.status(404).json(err)
            var to              = data.email
            var from            = 'abc-todo@example.com'
            var subject         = 'Change your password for abc-todo';

            var link            = "http://"+req.get('host')+"/api/user/forget/"+token;
            var html            = 'Plese click link bellow, if you register at todoglint.com<br>';
                html            += '<br><strong><a href='+link+'>'+"Reset Password"+'</a></strong>';
                html            += '<br><br>Thanks';
                
            funcHelper.mail(to, from, subject, html)
            res.status(200).json(success(data, "check your email to change your password!"))
        })
    },
    changePassword(req, res){
        let token = req.params.token;

        User.findOne({ token: token }, 'expToken').exec()
            .then((users)=>{
                if(Date.now()<users.expToken){
                    User.findOneAndUpdate({token: req.params.token}, {password: req.body.password}, (err, data)=>{
                        if (!data) return res.status(400).json(error("the token is not exist"))
                        res.status(200).json(success(data, "password change success"))
                  })
                }
            })
            .catch((err)=>{
                res.status(422).json(err)
            })
    }
}