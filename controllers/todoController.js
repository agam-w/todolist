const Todo = require('../models/todo')
const User = require('../models/user')
const validator = require('../middlewares/validator')
const {success, error} = require('../middlewares/response')

module.exports = {
    create(req, res){
        let isValid=validator.create(req.body)
        if (isValid !== true){
        return res.status(400).json(error(isValid[0].message))
        } 
        Todo.create(req.body, (err, data)=>{
            if (err) return res.status(422).json(error(err.message))
            User.findById(req.user._id, (err, user)=>{
                //if (err) return res.status(400).json(error("can't find user"))
                user.todos.push(data)
                user.save()
                data.user=req.user._id
                data.save()
            })
            res.status(201).json(success(data, "Todo created"))
        })
    },
    update(req, res){
        let isValid=validator.update(req.body)
        if (isValid !== true){
        return res.status(400).json(error(isValid[0].message))
        }

        Todo.findOne({_id: req.params.id}, (err, data)=>{
            if (!data || data.user._id != req.user._id) 
            return res.status(404).json(error("this is not your todolist!"))
            Todo.findByIdAndUpdate(req.params.id, req.body, (err, valid)=>{
                //if (err) return res.status(400).json(error(err.message))
                Todo.findById(req.params.id, (err, data)=>{
                    res.status(200).json(success(data, "Todo updated"))
                })
            }) 
        })
    },
    show(req, res){
        Todo.findById(req.params.id, (err, data)=>{
            if (!data) return res.status(403).json(error(err))
            if (data.user._id != req.user._id) return res.status(404).json(error("this is not your todolist!"))
            res.status(200).json(success(data, "show post"))
        })  
    },
    delete(req, res){
        Todo.findById(req.params.id).exec ((err, data)=>{
            if (!data || data.user != req.user._id) 
            return res.status(404).json(error("this is not your todolist!"))
            Todo.findByIdAndDelete(data._id, (err, todo)=>{

                //if (err) return res.status(422).json(error("there is nothing to delete"))
                User.update({ _id: req.user._id }, { $pullAll: { todos: [req.params.id] } }, 
                    { safe: true, multi:true }, function(err, obj) {

                        //if (err) return res.status(422).json(error("can't delete from array"))
                        res.status(200).json(success(todo ,"post deleted"))
                });
                
            })
        }) 
    },
    showAll(req, res){
        Todo.find({user:req.user._id}, (err, data)=>{
            //if (err) return res.status(400).json(error(err.message))
            res.status(200).json(success(data, "Showing All Todo"))
        })  
    }
}