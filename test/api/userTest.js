process.env.NODE_ENV = 'test'

const User = require('../../models/user')

const server = require('../../index.js')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect()
const chaiHttp = require('chai-http')
const faker = require('faker')

chai.use(chaiHttp)


var test =  {name   : faker.name.findName(),
    email   : faker.internet.email(),
    password: faker.internet.password()}

beforeEach((done)=>{
    User.create(test, (err, data)=>{
        if(err) console.log(err)
        done()
    })
    })
    
afterEach((done)=>{
    User.remove({}, (err)=>{
        done()
    })
})

describe ('POST /api/user',()=>{

    it("should create user", done=>{
        let test2 =  {name   : faker.name.findName(),
                     email   : faker.internet.email(),
                     password: faker.internet.password()}
        chai.request(server)
            .post('/api/user/')
            .send(test2)
            .end((err,res)=>{
                res.should.have.status(201)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('result')
                done()
        })
    })
})


describe ('POST /api/user/login',()=>{
    it("should login to user", done=>{
        chai.request(server).post('/api/user/login').send(test)
        .end((err,res)=>{
            res.should.have.status(200)
            res.body.should.have.property('success').equal(true)
            res.body.should.have.property('result')
            done()
        })    
    })
})

//ERROR TESTING
describe ('POST /api/user/',()=>{
    it("cannot create user", done=>{
        chai.request(server).post('/api/user/').send(test)
            .end((err,res)=>{
                res.should.have.status(422)
                done()
        })
    })
})

describe ('POST /api/user/login',()=>{
    it("cannot login because invalid password", done=>{
        let test3 = {email   : test.email,
                    password : faker.internet.password()}
        chai.request(server).post('/api/user/login').send(test3)
            .end((err,res)=>{
                res.should.have.status(403)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('result').equal("incorrect password")
                done()
        })
    })
})

describe ('POST /api/user/login',()=>{
    it("cannot login because invalid email", done=>{
        let test3 = {email   : faker.internet.email(),
                    password : test.password}
        chai.request(server).post('/api/user/login').send(test3)
            .end((err,res)=>{
                res.should.have.status(403)
                // err.body.should.be.an('object')
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('result').equal("invalid email")
                done()
        })
    })
})