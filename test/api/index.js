process.env.NODE_ENV = 'test';

const server = require('../../index');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

chai.use(chaiHttp);

describe('/GET root path', ()=>{
    it("Should return true in root path", done=>{
        chai.request(server)
    .get('/')
    .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('success').equal(true)
    done();
        })
    })  
})