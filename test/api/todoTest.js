process.env.NODE_ENV = 'test'

const User = require('../../models/user')
const Todo = require('../../models/todo')

const server = require('../../index.js')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect()
const chaiHttp = require('chai-http')
const faker = require('faker')
const jwt = require('jsonwebtoken');

chai.use(chaiHttp)

var user =  {name   : faker.name.findName(),
            email   : faker.internet.email(),
            password: faker.internet.password()}


var test =  {title  : faker.random.words(),
            body    : faker.random.words(),
            isDone  : false}

describe ('POST /api/user',()=>{

    beforeEach((done)=>{
        User.create(user, (err, data)=>{
            if(err) console.log(err)
            xxx = data
            done()
        })
    })
    
    beforeEach(done => {
        chai.request(server)
            .post('/api/user/login')
            .send(user)
            .end(function (err, res) {
                token = res.body.result
                done()
            })
    })

    after((done)=>{
        Todo.remove({}, (err)=>{
            done()
        })
    })

    it("should show all todolist", done=>{
        chai.request(server)
            .get('/api/todo')
            .set('authorization', token)
            .end((err,res)=>{
                res.should.have.status(200)
                done()
        })
    })

    it("should create todolist", done=>{
        
        chai.request(server)
            .post('/api/todo/')
            .send(test)
            .set('authorization', token)
            .end((err,res)=>{
                res.should.have.status(201)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('result')
                done()
        })
    })

    it("should update todolist", done=>{
        Todo.create({title: faker.random.words(),
                    user: xxx._id}, (err, data)=>{
                        if (err) console.log(err)
                        chai.request(server)
                            .put('/api/todo/'+ data._id)
                            .send({isDone: false})
                            .set('authorization', token)
                            .end((err,res)=>{
                                res.should.have.status(200)
                                res.body.should.have.property('success').equal(true)
                                res.body.should.have.property('result')
                                done()
                        })
                    })
    })

    it("should show selected todolist", done=>{
        Todo.create({title: faker.random.words(),
            user: xxx._id}, (err, data)=>{
                if (err) console.log(err)
                chai.request(server)
                    .get('/api/todo/'+ data._id)
                    .set('authorization', token)
                    .end((err,res)=>{
                        res.should.have.status(200)
                        res.body.should.have.property('success').equal(true)
                        res.body.should.have.property('result')
                        done()
                })
            })
    })

    it("should delete selected todolist", done=>{
        Todo.create({title: faker.random.words(),
            user: xxx._id}, (err, data)=>{
                if (err) console.log(err)
                chai.request(server)
                    .delete('/api/todo/'+ data._id)
                    .set('authorization', token)
                    .end((err,res)=>{
                        res.should.have.status(200)
                        res.body.should.have.property('success').equal(true)
                        res.body.should.have.property('result')
                        done()
                })
            })
    })

    it("should show all todolist", done=>{
        Todo.create({title: faker.random.words(),
            user: xxx._id}, (err, data)=>{
                if (err) console.log(err)
                chai.request(server)
                    .get('/api/todo/')
                    .set('authorization', token)
                    .end((err,res)=>{
                        res.should.have.status(200)
                        res.body.should.have.property('success').equal(true)
                        res.body.should.have.property('result')
                        done()
                })
            })
    })

//error testing
    it("should not create todolist because invalid token", done=>{
        chai.request(server)
            .post('/api/todo/')
            .send({title  : faker.random.words(),
                body    : faker.random.words(),
                isDone  : false})
            .set('authorization', 'fsfgfgdgfdddgfdgf')
            .end((err,res)=>{
                res.should.have.status(401)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('result').equal("Invalid Token")
                done()
        })
    })

    it("should not create todolist because validation", done=>{
        chai.request(server)
            .post('/api/todo/')
            .send({title  : faker.random.words(),
                  body    : faker.random.words(),
                  isDone  : "1234"})
            .set('authorization', token)
            .end((err,res)=>{
                res.should.have.status(400)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('result')
                done()
        })
    })

    it("should not create todolist because title is already exist", done=>{
        chai.request(server)
            .post('/api/todo/')
            .send(test)
            .set('authorization', token)
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('result')
                done()
        })
    })

    it("should not create todolist because do not have header ", done=>{
        chai.request(server)
            .post('/api/todo/')
            .send(test)
            .end((err,res)=>{
                res.should.have.status(401)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('result').equal("can't put from header")
                done()
        })
    })

    it("should not create todolist because title is already exist", done=>{
        chai.request(server)
            .post('/api/todo/')
            .send(test)
            .set('authorization', token)
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('result')
                done()
        })
    })


    it("should not update todolist because validation", done=>{
        Todo.create({title: faker.random.words(),
            user: xxx._id}, (err, data)=>{
                if (err) console.log(err)
                chai.request(server)
                    .put('/api/todo/'+ data._id)
                    .send({isDone: "falsex"})
                    .set('authorization', token)
                    .end((err,res)=>{
                        res.should.have.status(400)
                        res.body.should.have.property('success').equal(false)
                        res.body.should.have.property('result')
                        done()
                })
            })
    
    })

    it("should not update todolist because invalid user", done=>{
        Todo.create({title: faker.random.words(),
                    user: xxx._id}, (err, data)=>{
                        if (err) console.log(err)
                        var xyz = data
                        User.create({name   : faker.name.findName(),
                                    email   : faker.internet.email(),
                                    password: faker.internet.password()
                                },(err, user1)=>{
                                    if (err) return (err)
                                    jwt.sign({ _id: user1._id }, process.env.SECRET_KEY, function(err, token1) {
                                        chai.request(server)
                                        .put('/api/todo/'+ xyz._id)
                                        .send({isDone: false})
                                        .set('authorization', token1)
                                        .end((err,res)=>{
                                            res.should.have.status(404)
                                            res.body.should.have.property('success').equal(false)
                                            res.body.should.have.property('result').equal("this is not your todolist!")
                                            done()
                                    })
                                    });
                                })
                    })
      
    })

    it("should not show selected todolist because invalid user", done=>{
        Todo.create({title: faker.random.words(),
                    user: xxx._id}, (err, data)=>{
                        if (err) console.log(err)
                        var xyz = data
                        User.create({name   : faker.name.findName(),
                                    email   : faker.internet.email(),
                                    password: faker.internet.password()
                                },(err, user1)=>{
                                    if (err) return (err)
                                    jwt.sign({ _id: user1._id }, process.env.SECRET_KEY, function(err, token1) {
                                        chai.request(server)
                                        .get('/api/todo/'+ xyz._id)
                                        .set('authorization', token1)
                                        .end((err,res)=>{
                                            res.should.have.status(404)
                                            res.body.should.have.property('success').equal(false)
                                            res.body.should.have.property('result').equal("this is not your todolist!")
                                            done()
                                    })
                                    });
                                })
                    })
      
    })

    it("should not delete selected todolist because invalid user", done=>{
        Todo.create({title: faker.random.words(),
                    user: xxx._id}, (err, data)=>{
                        if (err) console.log(err)
                        var xyz = data
                        User.create({name   : faker.name.findName(),
                                    email   : faker.internet.email(),
                                    password: faker.internet.password()
                                },(err, user1)=>{
                                    if (err) return (err)
                                    jwt.sign({ _id: user1._id }, process.env.SECRET_KEY, function(err, token1) {
                                        chai.request(server)
                                        .delete('/api/todo/'+ xyz._id)
                                        .set('authorization', token1)
                                        .end((err,res)=>{
                                            res.should.have.status(404)
                                            res.body.should.have.property('success').equal(false)
                                            res.body.should.have.property('result').equal("this is not your todolist!")
                                            done()
                                    })
                                    });
                                })
                    })
      
    })

    it("should not show selected todolist because user been remove", done=>{
        Todo.create({title: faker.random.words(),
            user: xxx._id}, (err, data)=>{
                if (err) console.log(err)
                User.findByIdAndDelete(xxx.id).exec()
                chai.request(server)
                    .get('/api/todo/'+ data._id)
                    .set('authorization', token)
                    .end((err,res)=>{
                        res.should.have.status(403)
                        res.body.should.have.property('success').equal(false)
                        res.body.should.have.property('result').equal("there is no user found")
                        done()
                })
            })
    })
    
})