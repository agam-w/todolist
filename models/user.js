const mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator');
require('mongoose-type-email')
const Schema = mongoose.Schema

const userSchema = new Schema({

    name        : {type   :'string',
                  required: true,
                  unique  : true},
    email       : {type   :mongoose.SchemaTypes.Email,
                  required: true,
                  unique  : true},
    password    : {type   : 'string',
                  required: true},
    todos       : [{type  : Schema.Types.ObjectId, 
                  ref     : 'Todo'}],
    token       : {type   :'string',
                  required: false,
                  unique  : true},
    isVerified  : {type   : 'boolean', 
                  default : false},
    expToken    : {type   : 'date',
                  default : Date.now()}

  });

  userSchema.plugin(uniqueValidator);
  module.exports = mongoose.model('Users', userSchema);