const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema

const todoSchema = new Schema({

    title    :{type   :'string',
              required: true,
              unique  : true},
    body     :{type   :'string',
              required: false},
    isDone   :{type   : 'boolean',
              default : false},
    user     :{type   : Schema.Types.ObjectId, 
              ref     : 'User'}

  });

todoSchema.plugin(uniqueValidator);  
module.exports = mongoose.model('Todo', todoSchema);
